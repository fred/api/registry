if(DEFINED COMMON_CMAKE_66CB09A41A1A22D25C95E9A3F4BE6F6A)
    return()
endif()
set(COMMON_CMAKE_66CB09A41A1A22D25C95E9A3F4BE6F6A 1)

set(API_REGISTRY_VERSION "7.0.0")
message(STATUS "API_REGISTRY_VERSION: ${API_REGISTRY_VERSION}")

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}")

set(PROTOS_DIR ${CMAKE_CURRENT_LIST_DIR}/..)
set(PROTOBUF_IMPORT_DIRS ${PROTOS_DIR})

set(FRED_REGISTRY_GRPC_FILES
    ${PROTOS_DIR}/fred_api/registry/contact/service_contact_grpc.proto
    ${PROTOS_DIR}/fred_api/registry/contact/service_search_contact_grpc.proto
    ${PROTOS_DIR}/fred_api/registry/contact_representative/service_contact_representative_grpc.proto
    ${PROTOS_DIR}/fred_api/registry/domain/service_admin_grpc.proto
    ${PROTOS_DIR}/fred_api/registry/domain/service_domain_grpc.proto
    ${PROTOS_DIR}/fred_api/registry/domain/service_search_domain_grpc.proto
    ${PROTOS_DIR}/fred_api/registry/domain_blacklist/service_domain_blacklist_grpc.proto
    ${PROTOS_DIR}/fred_api/registry/keyset/service_keyset_grpc.proto
    ${PROTOS_DIR}/fred_api/registry/keyset/service_search_keyset_grpc.proto
    ${PROTOS_DIR}/fred_api/registry/nsset/service_nsset_grpc.proto
    ${PROTOS_DIR}/fred_api/registry/nsset/service_search_nsset_grpc.proto
    ${PROTOS_DIR}/fred_api/registry/registrar/service_admin_grpc.proto
    ${PROTOS_DIR}/fred_api/registry/registrar/service_registrar_grpc.proto)

set(FRED_REGISTRY_PROTO_FILES
    ${PROTOS_DIR}/fred_api/registry/common_types.proto
    ${PROTOS_DIR}/fred_api/registry/object_events.proto
    ${PROTOS_DIR}/fred_api/registry/pagination.proto
    ${PROTOS_DIR}/fred_api/registry/contact/add_contact_auth_info_types.proto
    ${PROTOS_DIR}/fred_api/registry/contact/contact_common_types.proto
    ${PROTOS_DIR}/fred_api/registry/contact/contact_handle_history_types.proto
    ${PROTOS_DIR}/fred_api/registry/contact/contact_history_types.proto
    ${PROTOS_DIR}/fred_api/registry/contact/contact_info_types.proto
    ${PROTOS_DIR}/fred_api/registry/contact/contact_state_flags_types.proto
    ${PROTOS_DIR}/fred_api/registry/contact/contact_state_history_types.proto
    ${PROTOS_DIR}/fred_api/registry/contact/contact_state_types.proto
    ${PROTOS_DIR}/fred_api/registry/contact/create_contact_types.proto
    ${PROTOS_DIR}/fred_api/registry/contact/list_contacts_types.proto
    ${PROTOS_DIR}/fred_api/registry/contact/list_merge_candidates_types.proto
    ${PROTOS_DIR}/fred_api/registry/contact/merge_contacts_types.proto
    ${PROTOS_DIR}/fred_api/registry/contact/update_contact_types.proto
    ${PROTOS_DIR}/fred_api/registry/contact/update_contact_state_stream_types.proto
    ${PROTOS_DIR}/fred_api/registry/contact/update_contact_state_types.proto
    ${PROTOS_DIR}/fred_api/registry/contact_representative/contact_representative_common_types.proto
    ${PROTOS_DIR}/fred_api/registry/domain/add_domain_auth_info_types.proto
    ${PROTOS_DIR}/fred_api/registry/domain/domain_common_types.proto
    ${PROTOS_DIR}/fred_api/registry/domain/domain_history_types.proto
    ${PROTOS_DIR}/fred_api/registry/domain/domain_info_types.proto
    ${PROTOS_DIR}/fred_api/registry/domain/domain_life_cycle_stage_types.proto
    ${PROTOS_DIR}/fred_api/registry/domain/domain_state_flags_types.proto
    ${PROTOS_DIR}/fred_api/registry/domain/domain_state_history_types.proto
    ${PROTOS_DIR}/fred_api/registry/domain/domain_state_types.proto
    ${PROTOS_DIR}/fred_api/registry/domain/domains_by_contact_types.proto
    ${PROTOS_DIR}/fred_api/registry/domain/fqdn_history_types.proto
    ${PROTOS_DIR}/fred_api/registry/domain/list_domains_types.proto
    ${PROTOS_DIR}/fred_api/registry/domain/list_domains_by_contact_types.proto
    ${PROTOS_DIR}/fred_api/registry/domain/list_domains_by_keyset_types.proto
    ${PROTOS_DIR}/fred_api/registry/domain/list_domains_by_nsset_types.proto
    ${PROTOS_DIR}/fred_api/registry/domain/update_domain_state_types.proto
    ${PROTOS_DIR}/fred_api/registry/domain_blacklist/domain_blacklist_common_types.proto
    ${PROTOS_DIR}/fred_api/registry/keyset/add_keyset_auth_info_types.proto
    ${PROTOS_DIR}/fred_api/registry/keyset/keyset_common_types.proto
    ${PROTOS_DIR}/fred_api/registry/keyset/keyset_handle_history_types.proto
    ${PROTOS_DIR}/fred_api/registry/keyset/keyset_history_types.proto
    ${PROTOS_DIR}/fred_api/registry/keyset/keyset_info_types.proto
    ${PROTOS_DIR}/fred_api/registry/keyset/keyset_state_flags_types.proto
    ${PROTOS_DIR}/fred_api/registry/keyset/keyset_state_history_types.proto
    ${PROTOS_DIR}/fred_api/registry/keyset/keyset_state_types.proto
    ${PROTOS_DIR}/fred_api/registry/keyset/list_keysets_by_contact_types.proto
    ${PROTOS_DIR}/fred_api/registry/keyset/update_keyset_state_types.proto
    ${PROTOS_DIR}/fred_api/registry/nsset/add_nsset_auth_info_types.proto
    ${PROTOS_DIR}/fred_api/registry/nsset/list_nssets_by_contact_types.proto
    ${PROTOS_DIR}/fred_api/registry/nsset/list_nssets_types.proto
    ${PROTOS_DIR}/fred_api/registry/nsset/nsset_common_types.proto
    ${PROTOS_DIR}/fred_api/registry/nsset/nsset_handle_history_types.proto
    ${PROTOS_DIR}/fred_api/registry/nsset/nsset_history_types.proto
    ${PROTOS_DIR}/fred_api/registry/nsset/nsset_info_types.proto
    ${PROTOS_DIR}/fred_api/registry/nsset/nsset_state_flags_types.proto
    ${PROTOS_DIR}/fred_api/registry/nsset/nsset_state_history_types.proto
    ${PROTOS_DIR}/fred_api/registry/nsset/nsset_state_types.proto
    ${PROTOS_DIR}/fred_api/registry/nsset/update_nsset_state_types.proto
    ${PROTOS_DIR}/fred_api/registry/registrar/registrar_certification_types.proto
    ${PROTOS_DIR}/fred_api/registry/registrar/registrar_common_types.proto
    ${PROTOS_DIR}/fred_api/registry/registrar/registrar_credit_types.proto
    ${PROTOS_DIR}/fred_api/registry/registrar/registrar_epp_credentials_types.proto
    ${PROTOS_DIR}/fred_api/registry/registrar/registrar_group_types.proto
    ${PROTOS_DIR}/fred_api/registry/registrar/registrar_info_types.proto
    ${PROTOS_DIR}/fred_api/registry/registrar/registrar_list_types.proto
    ${PROTOS_DIR}/fred_api/registry/registrar/registrar_zone_access_types.proto
    ${FRED_REGISTRY_GRPC_FILES})

set(FRED_REGISTRY_GENERATE_INTO ${CMAKE_CURRENT_BINARY_DIR})

set(PROTOBUF_PROTOC_OPTIONS --experimental_allow_proto3_optional=yes)
set(GRPC_PROTOC_OPTIONS --experimental_allow_proto3_optional=yes)
