ChangeLog
=========

.. contents:: Releases
   :backlinks: none
   :local:

Unreleased
----------

7.0.0 (2025-02-17)
------------------

* Add methods to add object auth infos (#90).
* Add ``SnapshotId`` to info and list methods (#92).
* Add methods to update object state (#88).
* Update ``update_contact`` method (#91).
* Cleanup ``DoesNotExist`` exceptions (#93).

6.0.0 (2024-11-20)
------------------

* Drop support for python 3.8.
* Add support for python 3.13.
* Add ``Keyset.batch_keyset_info`` method (#71).
* Add ``RegistrarRef`` with basic registrar reference fields (#70).
* Add pagination to ``Domain.list_domains_by_nsset`` (#68).
* Add ``Keyset.list_keysets_by_contact`` method (#67).
* Add ``Nsset.list_nssets_by_contact`` method (#66).
* Add ``Domain.list_domains_by_keyset`` method (#69).
* Add ``Domain.list_domains_by_contact`` method (#74, #82).
* Add ``Contact.list_merge_candidates`` method (#77).
* Add ``Contact.merge_contacts`` method (#78).
* Remove ``Domain.get_domains_by_holder`` method (#79).
* Add ``Nsset.batch_nsset_info`` method (#72).
* Add ``Domain.batch_domain_info`` method (#73).
* Add ``InvalidData`` exception to ``DomainInfoResponse`` response (#84).
* Move ``DomainInfo`` exception to common types (#83).
* Deprecate old domain list methods (#75).
* Drop ``auth_info`` from info replies (#37).
* Clear ``BatchDeleteDomainsReply`` (#6).
* Update project setup.

5.6.2 (2024-11-04)
------------------

* Add linked domains filter to ``list_nssets`` (#80).

5.6.1 (2024-08-19)
------------------

* Add ``InvalidData`` exception to ``ListDomainsByNsset`` response (#65).

5.6.0 (2024-08-09)
------------------

* Add ``create_contact`` method (#62).

5.5.5 (2024-07-30)
------------------

* Fix ``list_contacts`` reply to return stream (#61).

5.5.4 (2024-07-23)
------------------

* Fix ``ListContactsReply`` message (#60).

5.5.3 (2024-07-22)
------------------

* Add ``Contact.list_contacts`` method (#57).
* Add ``Domain.list_domains_by_nsset`` method (#59).

5.5.2 (2024-07-16)
------------------

* Add ``Nsset.list_nssets`` method (#58).

5.5.1 (2024-03-11)
------------------

* Add ``zone`` to ``list_domains`` (#54).

5.5.0 (2024-02-26)
------------------

* Add domain blacklist service (#47).
* The field ``blacklisted`` in ``DomainLifeCycleStageReply`` now returns list of block IDs (#48).
* Domain - add ``list_domains`` to list all registered domains (#52).
* Fix update registrar to accept registrar handle instead of id (#51).
* Add registrar id to ``RegistrarInfoReply`` (#49).

5.4.3 (2023-11-23)
------------------

* Add support for python 3.12.
* Add ``blacklisted`` to ``DomainLifeCycleStageReply`` (#46).
* Update docs of ``get_domain_life_cycle_stage``.

5.4.2 (2023-09-20)
------------------

* Drop support for python 3.7.
* Add ``in_auction`` to ``DomainLifeCycleStageReply`` (#44).
* Update project setup.

5.4.1 (2023-09-20)
------------------

* Fix cmake build options

5.4.0 (2023-06-06)
------------------

New features

* Add new service for contact representative (#35, #36).

Maintenance

* Update ``.gitignore``.

5.3.0 (2023-01-18)
------------------

* Add related object refs (#29).
* Add real time of outzone and delete events (#27).

5.2.4 (2023-01-16)
------------------

* Add ``get_domain_life_cycle_stage`` function (#28).
* Update project setup.

5.2.3 (2023-01-10)
------------------

* Add deleted domains to ``get_domains_by_contact`` result (#26).

5.2.2 (2022-12-07)
------------------

* Add support for python 3.11.
* Add ENUM domain validation expiration (#25).

5.2.1 (2022-11-30)
------------------

* Add check dns host rpc (#24).

5.2.0 (2022-11-18)
------------------

New features

* Add domain lifecycle milestones
* Add registrar handle to ``update_registrar`` and allow registrar handle change

5.1.0 (2022-09-01)
------------------

New features

* Add methods for editing registrar info (``create_registrar`` and ``update_registrar``)
* Add methods for editing registrar zone access
* Add methods for editing registrar EPP access
* Add methods for editing registrar certification
* Add methods for editing registrar group membership
* Add zone access history to ``get_registrar_zone_access``
* Add ``is_internal`` flag to registrar info

Maintenance

* Use ``setuptools-grpc``

5.0.6 (2022-05-04)
------------------

Bug fixes

* Split notify info function and replace date with two timestamps
* Domain history id is now optional in manage_domain_state_flags and throws exception on mismatch

5.0.5 (2022-04-27)
------------------

New features

* Add get_domains_by_contact function
* Add domain contact info notify methods
* Add sponsoring_registrar into InfoReply messages

5.0.4 (2022-03-30)
------------------

Bugfixes

* Fix project build with pyproject.toml

5.0.3 (2022-03-17)
------------------

New features

* Add ``get_<object>_id`` functions.

Other

* Update project setup

5.0.2 (2022-02-17)
------------------

New Features

* Add ``LogEntryId`` to registrable objects (domain, contact, nsset, keyset) history records
* Drop support for python 3.5 and 3.6.
* Add support for python 3.9 and 3.10.
* Update project setup.


5.0.1 (2022-01-20)
------------------

Bug fixes

* Fix protobuf and grpc cmake build targets


5.0.0 (2021-08-04)
------------------

Breaking changes

* Remove diagnostics service (use Fred.Diagnostics instead)

New features

* Add domain batch functions

Bug fixes

* Fix protobuf dependencies
* Unpin grpc setup dependencies

Docs

* Update README and CHANGELOG according to our internal standard


4.5.0 (2020-03-13)
------------------

New features

* Add LogEntryId into update operations


4.4.1 (2020-03-05)
------------------

Bug fixes

* Fix Python package versioning


4.4.0 (2020-02-26)
------------------

New features

* Add contact update
* Add contact state update


4.3.1 (2020-02-04)
------------------

Bug fixes

* Pin grpcio-tools version in setup dependencies


4.3.0 (2020-01-27)
------------------

New features

* Add search domain history
* Add search keyset history
* Add search nsset history


4.2.0 (2019-12-05)
------------------

New features

* Add search contact history


4.1.1 (2019-09-18)
------------------

Bug fixes

* Remove useless includes


4.1.0 (2019-08-01)
------------------

New features

* Add diagnostic service


4.0.0 (2019-06-19)
------------------

Breaking changes

* Change contact search to look for multiple items

New features

* Add keyset service
* Add keyset search
* Add domain search
* Add nsset search
* Add method for getting registrar credit
* Add method for getting registrar zone access


3.0.0 (2019-05-21)
------------------

Breaking changes

* Rename some contact types
* Change single-value e-mail field to array
* Unify all replies to contain ``data`` item
* Remove ``numeric_id`` item from object infos

New features

* Add nsset service
* Add domain history methods
* Add registrar service


2.0.0 (2019-04-15)
------------------

Breaking changes

* Rename some structures in contact API

New features

* Add domain state and info methods
* Change automatic Python gRPC module build to produce namespace packages


1.0.0 (2019-03-14)
------------------

Initial version.

* Add automatic Python gRPC modules build to setup.py
* Add contact state, info and history methods
* Add contact search
